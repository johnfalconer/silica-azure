#/bin/bash
#
####################################
#
#  start  the silica-app by applying the kubernetes deployments specified in silica directoy .yaml files
#  these will create a deployment containing one pod running the silica app plus a service to handle cluster and internet requests
#
cd $HOME
kubectl apply -f silica