#/bin/bash
#
####################################
#
#  start  the Neo4j database  by applying the kubernetes deployments specified in neo4j  directoy .yaml files
#  these will create a deployment containing one pod running the neo4j app plus a service to handle cluster and internet requests
#
cd $HOME
kubectl apply -f neo4j
